package org.example;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;


public class StartPage {
    public WebDriver driver;

    public StartPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(css = "body > div.rows-wrapper > div.container.rows > div.row.rows__row.rows__row_first > div > div > div > div.col.headline__item.headline__leftcorner > a")
    private WebElement locate;

    public void clickLocate(){
        locate.click();
    }

    @FindBy(xpath = "//*[@id=\"city__front-input\"]")
    private WebElement inputLocate;


    public void inputLocate(String city) throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"form__a11y\"]/div[3]/div/div[2]/div[1]/span/span")).click();

        inputLocate.clear();
        inputLocate.sendKeys(city);
        Thread.sleep(5000);
        inputLocate.sendKeys(Keys.ENTER);
    }

    @FindBy(css = "body > div.rows-wrapper > div.container.rows > div.row.rows__row.rows__row_main > div > div.container.container__services.container__line > div > div > a")
    private WebElement moreBtn;

    public void clickMore(){
        moreBtn.click();
    }

}

