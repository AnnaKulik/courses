package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Tests {
    public static WebDriver driver;
    public static StartPage startPage;
    public static LocatePage locatePage;
    @BeforeMethod
    public static void setup(){
        System.setProperty("webdriver.chrome.driver", ConfProperties.getProperty("chromedriver"));
        driver = new ChromeDriver();
        startPage = new StartPage(driver);
        LocatePage locatePage = new LocatePage(driver);
        //окно разворачивается на полный экран
        //driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //driver.get(ConfProperties.getProperty("page"));
        driver.get(ConfProperties.getProperty("page"));
    }

    @Test
    public void FormationStructProject() throws InterruptedException {
        startPage.clickLocate();
        startPage.inputLocate("Лондон великобритания");
        List<WebElement> moreListLondon = driver.findElements(
                By.cssSelector("body > div.popup2.services-new__more-popup.popup2_view_default.popup2_target_anchor.popup2_autoclosable_yes.i-bem.popup2_js_inited.popup2_direction_bottom-left.popup2_visible_yes > div > div.services-new__more-popup-services > a:not([style*=\"display: none\"])\n")
        );
        startPage.clickLocate();
        startPage.inputLocate("Париж франиця");
        List<WebElement> moreListParis = driver.findElements(
                By.cssSelector("body > div.popup2.services-new__more-popup.popup2_view_default.popup2_target_anchor.popup2_autoclosable_yes.i-bem.popup2_js_inited.popup2_direction_bottom-left.popup2_visible_yes > div > div.services-new__more-popup-services > a:not([style*=\"display: none\"])\n")
        );
        Assert.assertEquals(moreListParis,moreListLondon);
    }
}
