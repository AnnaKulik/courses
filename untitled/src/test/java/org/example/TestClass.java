package org.example;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.*;

public class TestClass {
    ClassForActualValue classForActualValue = new ClassForActualValue(5);
    ClassForActualValue classForActualValueStr = new ClassForActualValue("Kulik");
    ClassForActualValue classForActualValueIntArr = new ClassForActualValue(1,2,3);
    ClassForActualValue classForActualValueStrArr = new ClassForActualValue ("Kulik", "Anna", "Andrianovna");

    @Test
    public void testInt(){
        Assert.assertEquals(classForActualValue.intValue,5);
    }

    @Test
    public void testStringEq(){
        Assert.assertEquals(classForActualValueStr.strValue,"Kulik");
    }

    @Test
    public void testStringEqPart(){
        String kulik= "AnnaKulikAnna";
        Assert.assertTrue(kulik.contains(classForActualValueStr.strValue));
    }

    int[] array= new int[]{1,2,3};
    @Test
    public void testIntArr(){
        Assert.assertEquals(classForActualValueIntArr.arrayInt, array );
    }

    String[] arrayStr = new String[]{"Kulik", "Anna", "Andrianovna"};
    @Test
    public void testStrArr(){
        Assert.assertEquals(classForActualValueStrArr.arrayStr, arrayStr );
    }
    public static void main(String[] args) {

        System.setProperty("webdriver.chrome.driver", "C:/Users/panas/Automation/COURSES/Java/courses/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("http://www.google.com");

    }
}
