package org.example;

public class ClassForActualValue {
    public int intValue;
    public String strValue;
    public int[] arrayInt;
    public String[] arrayStr;

    ClassForActualValue(int intValue){
        this.intValue=intValue;
    }

    ClassForActualValue(int intValue, int intValue1, int intValue2){
        this.arrayInt= new int[] {intValue, intValue1, intValue2};
    }

    ClassForActualValue(String str){
        this.strValue=str;
    }

    ClassForActualValue(String str, String str2, String str3){
        this.arrayStr= new String[] {str, str2, str3};
    }

}
