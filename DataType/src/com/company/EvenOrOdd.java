package com.company;

import java.util.Scanner;

public class EvenOrOdd {
    public static void evenOrOdd() {
        int n;

        Scanner in = new Scanner(System.in);
        System.out.print("Введи натуральное число:");
        n = in.nextInt();

        if (n % 2 == 0) {
            System.out.print("Ваше число четное");
        } else {
            System.out.print("Ваше число не четное");
        }

    }
}
