package com.company;

import java.util.Scanner;

public class CoastOfProduct {
    public static void coastOfProduct() {
        Product products[] = {
                new Product("01", "Ice-cream", 10.00),
                new Product("02", "Milk", 1.00),
                new Product("03", "Chocolate", 15.25)
        };

        Scanner in = new Scanner(System.in);
        System.out.print("Введи код продукта:");
        String code = in.next();
        Product foundProduct = null;

        for (Product product : products) {
            if (product.getCode().equals(code)) {
                foundProduct = product;
            }
        }

        if (foundProduct != null) {
            System.out.printf("Наименование: %s, цена: %.2f\n", foundProduct.getName(), foundProduct.getCoast());
        } else {
            System.out.printf("Товара не существует");
        }

    }
}
