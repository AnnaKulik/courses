package com.company;

import java.util.Scanner;

public class WhatToDo {
    static void whatToDo() {
        int time, money;
        String answer;

        Scanner in = new Scanner(System.in);

        System.out.print("Укажите время:");
        time = in.nextInt();

        System.out.print("Укажите количество денег:");
        money = in.nextInt();

        if ((time >= 8 && time <= 12) && money > 10) {
            answer = "Иди в магазин";
        } else if (time > 12 && time < 19) {
            if (money > 50) {
                answer = "Иди в кафе";
            } else {
                answer = "Иди к соседу";
            }
        } else if (time > 19 && time < 22) {
            answer = "Смотри телик";
        } else if (time > 22) {
            answer = "Спи";
        } else {
            answer = "Делай что хочешь, я тебе не подскажу";
        }
        System.out.print(answer);
    }
}
