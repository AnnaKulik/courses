package com.company;

public class ArrayToString {
    static void arrayToString() {
        char array[] = {'A', 'b', '1', '/', 'f', '3', 'f', 'q'};
        String arrayToString = new String(array);

        System.out.print("Существующий массив:");

        for (int i = 0; i < array.length; i++) {
            System.out.printf("%c ", array[i]);
        }

        System.out.println("\nПреобразван в строку:" + arrayToString);

        char newArray[] = arrayToString.toCharArray();
        System.out.print("Преобразваная строка в массив: ");
        for (int i = 0; i < newArray.length; i++) {
            System.out.printf("%c ", newArray[i]);
        }
    }
}
