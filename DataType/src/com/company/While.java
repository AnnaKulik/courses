package com.company;

public class While {
    static void cicle() {
        int a = 15;

        System.out.println("Пред условие ");
        while (a >= 10) {
            System.out.print(" " + a);
            a--;
        }

        a = 15;
        System.out.println("\n Пост условие ");
        do {
            System.out.print(" " + a);
            a--;
        } while (a >= 10);
    }
}
