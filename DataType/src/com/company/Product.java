package com.company;

public class Product {
    private String code;
    private String name;
    private double coast;

    Product(String code, String name, double coast) {
        this.code = code;
        this.name = name;
        this.coast = coast;
    }

    void display() {
        System.out.printf(" Name: %s \n", this.name);
    }

    double getCoast() {
        return this.coast;
    }

    String getName() {
        return this.name;
    }

    public String getCode() {
        return this.code;
    }
}
