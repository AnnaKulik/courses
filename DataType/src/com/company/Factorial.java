package com.company;

import java.util.Scanner;

public class Factorial {

    static int factorial(int n) {
        int factorial = 1;
        for (int i = 1; i <= n; i++) {
            factorial *= i;
        }
        return factorial;
        //System.out.print(+factorial);
    }

    static void factorial() {
        int n, fact = 1;

        Scanner in = new Scanner(System.in);
        System.out.print("Введи натуральное число:");
        n = in.nextInt();

        for (int i = 1; i <= n; i++) {
            fact = fact * i;
        }
        System.out.print("Факториал для:" + n + "=" + fact);
    }
}
