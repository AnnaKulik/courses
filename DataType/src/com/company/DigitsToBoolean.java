package com.company;

public class DigitsToBoolean {
    public static void digitsToBoolean() {
        int arr[] = {0, 1, 2, -1, 0, 3};
        boolean arrBoolean[] = new boolean[arr.length];

        for (int i = 0; i < arr.length; i++) {
            arrBoolean[i] = arr[i] != 0;
            System.out.println(arrBoolean[i]);
        }
    }
}
