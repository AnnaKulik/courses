package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int programm;
        Scanner in = new Scanner(System.in);
        System.out.print("Выбери программу для запуска:\n" +
                "1. Посчитать среднее арифметическое массива.\n" +
                "2. Факториал числа. Вычисляем через метод без параметров\n" +
                "3. Вывести цену за определенный товар опираясь на его код\n" +
                "(предусмотреть возможность введение неверного кода).\n" +
                "4. Вывести на экран информацию о том является ли целое\n" +
                "число записанное в переменную n, чётным либо нечётным.\n" +
                "5. Перобразовать массив в строку и обртано в массив \n" +
                "6. Добавить к каждому элементу массива слова hello \n" +
                "7. Преобразовать числовой массив в boolean\n" +
                "8. Подскажет что делать\n" +
                "9. Вычиляем факториал через метод с параметрами\n" +
                "10. Циклы while\n" +
                "11. Цикл forach  для массива из 10 рандомных чисел,если число равно 5 вывести в консоль");
        programm = in.nextInt();
        switch (programm) {
            case 1:
                AverageOfArray.averageOfArray();
                break;
            case 2:
                Factorial.factorial();
                break;
            case 3:
                CoastOfProduct.coastOfProduct();
                break;
            case 4:
                EvenOrOdd.evenOrOdd();
                break;
            case 5:
                ArrayToString.arrayToString();
                break;
            case 6:
                AddHelloToElements.addHelloToElements();
                break;
            case 7:
                DigitsToBoolean.digitsToBoolean();
                break;
            case 8:
                WhatToDo.whatToDo();
                break;
            case 9:
                int n;
                System.out.println("Чило для кооторого высчитываем факториал");
                n = in.nextInt();
                System.out.println("Результат:" + Factorial.factorial(n));
                break;
            case 10:
                While.cicle();
                break;
            case 11:
                RandomArray.randomArray();
                break;
            default:
                System.out.print("Такой программы нет");
                break;
        }
    }
}
