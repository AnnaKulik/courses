package org.example;

import org.openqa.selenium.support.ui.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.By;


public class ResultOfSearch {
    public WebDriver driver;

    public ResultOfSearch(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@id=\"search-input\"]")
    private WebElement searchInput;

    @FindBy(xpath = "//*[@id=\"main-content\"]/div[3]/div/div/ul/li[1]/div/div/div[1]/div[1]/p[1]/a")
    private WebElement firstHref;

    @FindBy(xpath = "//*[@id=\"main-content\"]/div[3]/div/div/ul/li[1]/div/div/div[1]/div[1]/p[1]/a/span")
    private WebElement href;

    public String getTextOfSearch() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"search-input\"]")));
        String text = searchInput.getAttribute("value");
        return text;
    }

    public String getTextOfHref() {
        String text = href.getText();
        return text;
    }

    public void clickOnHref() {
        this.firstHref.click();
    }
}
