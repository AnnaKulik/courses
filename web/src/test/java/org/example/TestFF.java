package org.example;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestFF {
    public static WebDriver driverFF;
    public static LoginPage loginPageFF;
    public static Search searchFF;
    public static ResultOfSearch resultOfSearchFF;
    public static Href hrefFF;

    @BeforeClass
    public static void setup() {
        if (driverFF == null) {
            //определение пути до драйвера и его настройка
            System.setProperty("webdriver.gecko.driver", ConfProperties.getProperty("firefoxdriver"));
            //создание экземпляра драйвера
            driverFF = new FirefoxDriver();
            //окно разворачивается на полный экран
            //driver.manage().window().maximize();
            //задержка на выполнение теста = 10 сек.
            //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            //получение ссылки на страницу входа из файла настроек
            driverFF.get(ConfProperties.getProperty("page"));
        }

        loginPageFF = new LoginPage(driverFF);
        searchFF = new Search(driverFF);
        resultOfSearchFF = new ResultOfSearch(driverFF);
        hrefFF = new Href(driverFF);
    }

    @Test
    public void searchTestFF() {
        //нажимаем кнопку входа
        //loginPageFF.clickLoginBtn();
        // ввод в поле поиска
        searchFF.inputSearch(ConfProperties.getProperty("search"));
        //поиск
        searchFF.clickSearchBtn();
        String textSearch = resultOfSearchFF.getTextOfSearch();
        String textHref = resultOfSearchFF.getTextOfHref();
        Assert.assertEquals(ConfProperties.getProperty("search"), textSearch);
        resultOfSearchFF.clickOnHref();
        String textTitle = hrefFF.getTextOfTitle();
        textHref.contains(textTitle);
        searchFF.isSearch();
    }
}
