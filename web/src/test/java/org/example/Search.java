package org.example;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
public class Search {
    public WebDriver driver;

    public Search(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@id=\"orb-search-q\"]")
    private WebElement search;

    @FindBy(xpath = "//*[@id=\"orb-search-button\"]")
    private WebElement searchButton;

    public void inputSearch(String search) {
        this.search.sendKeys(search);
    }

    public void clickSearchBtn() {
        searchButton.click();
    }

    public void isSearch() {
        WebElement dynamicElement = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"orb-search-q\"]")));
        String display;
        if (search.isDisplayed()) {
            display = "true";
        } else {
            display = "false";
        }
        System.out.println("isDisplayed: " + display);

        String enabled;
        if (search.isEnabled()) {
            enabled = "true";
        } else {
            enabled = "false";
        }
        System.out.println("isEnabled: " + enabled);

    }

}
