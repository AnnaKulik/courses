package org.example;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.GeckoDriverService;




public class Testt {

    public static WebDriver driver;
    public static LoginPage loginPage;
    public static Search search;
    public static ResultOfSearch resultOfSearch;
    public static Href href;

    @BeforeClass
    public static void setup() {
        if (driver == null) {
            //определение пути до драйвера и его настройка
            System.setProperty("webdriver.chrome.driver", ConfProperties.getProperty("chromedriver"));
            //создание экземпляра драйвера
            driver = new ChromeDriver();
            //окно разворачивается на полный экран
            //driver.manage().window().maximize();
            //задержка на выполнение теста = 10 сек.
            //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            //получение ссылки на страницу входа из файла настроек
            driver.get(ConfProperties.getProperty("page"));
        }

        loginPage = new LoginPage(driver);
        search = new Search(driver);
        resultOfSearch = new ResultOfSearch(driver);
        href = new Href(driver);
    }


    @Test
    public void loginTest() {
        //нажимаем кнопку входа
        loginPage.clickLoginBtn();
    }

    @Test
    public void searchTest() {
        //нажимаем кнопку входа
        loginPage.clickLoginBtn();
        // ввод в поле поиска
        search.inputSearch(ConfProperties.getProperty("search"));
        //поиск
        search.clickSearchBtn();
        String textSearch = resultOfSearch.getTextOfSearch();
        String textHref = resultOfSearch.getTextOfHref();
        Assert.assertEquals(ConfProperties.getProperty("search"), textSearch);
        resultOfSearch.clickOnHref();
        String textTitle = href.getTextOfTitle();
        textHref.contains(textTitle);
        search.isSearch();

    }

}
