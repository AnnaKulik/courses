package PrinciplesOOP;

import PrinciplesOOP.Sobaka;

public class Dog extends Sobaka {
    @Override
    public void growth() {
        System.out.println("I'm big wolf");
    }

    public void growth(double n) {
        System.out.printf("I'm big wolf %f meters\n", n);
    }

    @Override
    public void weight() {
        System.out.println("I have weight");
    }

    public void weight(double n) {
        System.out.printf("I have %f futs", n);
    }

    @Override
    public void motion() {
        System.out.println("Ufff");
    }

    public void motion(String str) {
        System.out.println("I can " + str);
    }

    @Override
    public void eating() {
        System.out.println("I need more power");
    }
}
