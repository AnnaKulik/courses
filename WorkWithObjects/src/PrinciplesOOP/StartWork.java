package PrinciplesOOP;

import PrinciplesOOP.Dog;
import PrinciplesOOP.Sobaka;

public class StartWork {
    public static void main(String[] args) {
        System.out.println("Собака:");
        Sobaka sobaka = new Sobaka();
        sobaka.growth();
        sobaka.eating();
        sobaka.motion();
        sobaka.weight();

        System.out.println("\nДог:");
        Dog dog = new Dog();
        dog.growth();
        dog.eating();
        dog.motion();
        dog.weight();

        System.out.println("\nПерегруженный дог:");
        dog.growth(0.8);
        dog.motion("do auf");
        dog.weight(15.0);
    }
}

