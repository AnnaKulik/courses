package PrinciplesOOP;

import PrinciplesOOP.Animals;

public class Sobaka implements Animals {

    public void growth() {
        System.out.println("Рост собаки");
    }

    public void weight() {
        System.out.println("Вес собаки");
    }

    public void motion() {
        System.out.println("Собака бегает");
    }

    public void eating() {
        System.out.println("Кушает собака ротом");
    }
}
