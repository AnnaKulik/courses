package Exceptions;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class FileExcept {
    public static void main(String[] args) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("/home/nuta/automation/courses/file.txt"));
            String firstString = reader.readLine();
            System.out.println(firstString);
        } catch (IOException e) {
            System.out.println("Ошибка! Файл не найден!");
        }
    }
}
