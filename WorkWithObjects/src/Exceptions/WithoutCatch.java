package Exceptions;
import Modifires.Cats;

import java.util.Scanner;

public class WithoutCatch {
        public static void main(String[] args) {
            int firstValue, secondValue;
            double result = 0;
            Scanner in = new Scanner(System.in);
            System.out.print("Введи делимое число: \n");
            firstValue = in.nextInt();
            System.out.print("Введи делитель: \n");
            secondValue = in.nextInt();


            try {
                result = firstValue / secondValue;
            } finally {
                System.out.println("Вы пытались делить на ноль");
            }
        }
    }
