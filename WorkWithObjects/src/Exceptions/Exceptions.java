package Exceptions;

import java.util.Scanner;

public class Exceptions {
    public static void main(String[] args) {
        int firstValue, secondValue;
        double result = 0;
        Scanner in = new Scanner(System.in);
        System.out.print("Введи делимое число: \n");
        firstValue = in.nextInt();
        System.out.print("Введи делитель: \n");
        secondValue = in.nextInt();

        try {
            result = firstValue/secondValue;
        } catch (ArithmeticException e) {
            System.out.println("Вы пытались делить на ноль");
            result = 0.0;
        }

    }
}
