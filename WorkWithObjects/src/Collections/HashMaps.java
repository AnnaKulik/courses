package Collections;
import com.sun.xml.internal.ws.server.ServerSchemaValidationTube;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;


public class HashMaps {
    public static void main(String[] args) {
        HashMap<String,String> hashMap = new HashMap<String, String>();

        hashMap.put("19.10.1997","Anna");
        hashMap.put("13.12.1975","Zhanna");
        hashMap.put("19.10.1975","Andrian");

        System.out.println("Колекция MAP:");
        for (Map.Entry<String,String> item: hashMap.entrySet()){
            System.out.printf("Key: %s value: %s\n", item.getKey(), item.getValue());
        }

        LinkedHashSet<String> names = new LinkedHashSet<String>();

        names.add("Anna");
        names.add("Anna");

        System.out.println("\nКолекция Set:");
        for (String item: names) {
            System.out.print(item);
        }
    }
}
