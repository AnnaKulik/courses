package ForThread;

import java.util.HashMap;
import java.util.Map;

public class Sinchrone {
    public static void main(String[] args) {

        HashMap<String,String> hashMaps = new HashMap<String, String>();

        hashMaps.put("19.10.1997","Anna");
        hashMaps.put("13.12.1975","Zhanna");
        hashMaps.put("19.10.1975","Andrian");

        Thread t = new Thread(new MyThread(hashMaps));
        t.setName("First");
        t.start();
        Thread t2 = new Thread(new MyThread(hashMaps));
        t2.setName("Second");
        t2.start();
    }

}

class MyThread implements Runnable {
    HashMap<String,String> hashMaps;

    MyThread(HashMap<String,String> _hashMaps) {
        this.hashMaps = _hashMaps;
    }

    public void run() {
        synchronized (this.hashMaps) {
            for (Map.Entry<String, String> item : this.hashMaps.entrySet()) {
                System.out.printf("Key: %s value: %s thread: %s \n", item.getKey(), item.getValue(), Thread.currentThread().getName());

                try{
                    Thread.sleep(100);
                }
                catch(InterruptedException e){}

            }
        }
    }

}