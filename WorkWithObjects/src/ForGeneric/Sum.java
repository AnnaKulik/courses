package ForGeneric;

public class Sum <T>{

    T value;
    Sum(T val){
        value=val;
    }

    T getValue(){
        return value;
    }
    public static void main(String[] args) {
        Sum <Integer> sum = new Sum<Integer>(10);
        Sum <String> sum1 = new Sum<String>("десять");

        int num = sum.getValue();
        String str1=sum1.getValue();

        System.out.print("Число "+num+" пишется как "+str1);
    }
}
