package Modifires;

public class Cats {
    public String name;
    protected int age;
    private String kind;

    public Cats(String name, int age, String kind) {
        this.name = name;
        this.age = age;
        this.kind = kind;
    }

    public void displayName() {
        System.out.printf("Name: %s\n", name);
    }

    protected void displayAge() {
        System.out.printf("Age: %d\n", age);
    }

    private void lifespan() {
        System.out.println("Средняя продолжительность жизни 15 лет");
    }

    void lifespanForEveryOne() {
        System.out.printf("Средняя продолжительность жизни %d лет\n", age);
    }


    public String getName () {
        return name;
    }


}
