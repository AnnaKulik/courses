package Modifires;

public class Animal {

    public static void main(String[] args) {
        Cats cat = new Cats("Fee", 10, "Cat");
        System.out.println("Public and private package methods for cat");
        cat.displayName(); // public
        cat.lifespanForEveryOne(); //private package

        System.out.println("\nStatic method without object:");
        Lion.method();
    }
}
