package Modifires;

public class Lion extends Cats {
    public String prideName;

    Lion(String name, int age, String kind, String prideName) {
        super(name, age, kind);
        this.prideName = prideName;
    }

    public static void method() {
        System.out.printf("Кажде существо имеет продолжительность жизни");
    }

    public static void main(String[] args) {
        Cats cat = new Cats("Fee", 15, "Lion");
        Lion lion = new Lion("Lucky", 21, "Lion", "Supper");
        System.out.println("For cat:");
        cat.displayName(); // public
        cat.displayAge(); //protected

        System.out.println("\nFor lion:");
        lion.displayName(); // public
        lion.displayAge(); //protected
    }

}
